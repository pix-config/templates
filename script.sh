#!/bin/bash

#reset all
reset='\033[0m'

#formatting
defaulttxt="\e[0m"
bold="\e[1m"
nobold="\e[21m"
dim="\e[2m"
nodim="\e[22m"
underlined="\e[4m"
nounderlined="\e[24m"
blink="\e[5m"
noblink="\e[25m"
inverted="\e[7m"
noinverted="\e[27m"
hidden="\e[8m"
nohidden="\e[28m"

#foreground colors
defaultcol="\e[39m"
black="\e[30m"
white="\e[97m"
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
blue="\e[34m"
magenta="\e[35m"
cyan="\e[36m"
lightgray="\e[37m"
darkgray="\e[90m"
lightred="\e[91m"
lightgreen="\e[92m"
lightyellow="\e[93m"
lightblue="\e[94m"
lightmagenta="\e[95m"
lightcyan="\e[96m"

#background colors
defaultbg="\e[49m"
blackbg="\e[40m"
whitebg="\e[107m"
redbg="\e[41m"
greenbg="\e[42m"
yellowbg="\e[43m"
bluebg="\e[44m"
magentabg="\e[45m"
cyanbg="\e[46m"
lightgraybg="\e[47m"
darkgraybg="\e[100m"
lightredbg="\e[101m"
lightgreenbg="\e[102m"
lightyellowbg="\e[103m"
lightbluebg="\e[104m"
lightmagentabg="\e[105m"
lightcyanbg="\e[106m"

echo "pixy world!"
